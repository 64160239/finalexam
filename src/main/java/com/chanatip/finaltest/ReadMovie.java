/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chanatip.finaltest;

import com.chanatip.swingproject.Friend;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.stream.FileCacheImageInputStream;

/**
 *
 * @author chanatip
 */
public class ReadMovie {
    public static void main(String[] args) {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {          
            
            file = new File("movie.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            ArrayList<Movie> movie = (ArrayList<Movie>) ois.readObject();
            System.out.println(movie);
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadMovie.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReadMovie.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ReadMovie.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
