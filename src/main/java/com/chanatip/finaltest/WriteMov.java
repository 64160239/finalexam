/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chanatip.finaltest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author chanatip
 */
public class WriteMov {
    public static void main(String[] args) {
        ArrayList<Movie> movie = new ArrayList();
        movie.add(new Movie("Batman", "1h 55m", "Action"));
        movie.add(new Movie("Captain America 1 The Frist Avenger", "2h 4m", "Action"));
        movie.add(new Movie("Spirited away", "2h 5m", "Anime"));
        
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
          file =  new File("movie.bin");
          fos = new FileOutputStream(file);
          oos = new ObjectOutputStream(fos);
          oos.writeObject(movie);
          oos.close();
          fos.close();
          
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteMov.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteMov.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
