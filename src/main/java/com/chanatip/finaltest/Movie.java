/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chanatip.finaltest;

import java.io.Serializable;

/**
 *
 * @author chanatip
 */
public class Movie implements Serializable{
    private String name;
    private String time;
    private String type;

    @Override
    public String toString() {
        return "Movie{" + "Name:" + name + ", Time:" + time + ", Type:" + type + '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Movie(String name, String time, String type) {
        this.name = name;
        this.time = time;
        this.type = type;
    }
    
}
